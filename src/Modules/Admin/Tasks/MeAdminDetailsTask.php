<?php

namespace Sto\Modules\Admin\Tasks;

use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Services\Core\Task\Abstracts\Task;

class MeAdminDetailsTask extends Task
{
    private $authenticationService;

    public function __construct(
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param $authorizationHeader
     *
     * @return bool
     */
    public function run()
    {
        return $this->authenticationService->getAuthenticatedUser();
    }
}