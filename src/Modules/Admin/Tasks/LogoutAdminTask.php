<?php

namespace Sto\Modules\Admin\Tasks;

use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class LogoutTask.
 *
 * 
 */
class LogoutAdminTask extends Task
{

    /**
     * @var \Sto\Modules\Admin\Tasks\AuthenticationService|\Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * LogoutTask constructor.
     *
     * @param \Sto\Services\Authentication\Portals\AuthenticationService $authenticationService
     */
    public function __construct(
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param $authorizationHeader
     *
     * @return bool
     */
    public function run($authorizationHeader)
    {
        $ok = $this->authenticationService->logout($authorizationHeader);

        return $ok;
    }
}
