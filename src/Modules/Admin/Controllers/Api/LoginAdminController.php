<?php

namespace Sto\Modules\Admin\Controllers\Api;

use Sto\Modules\Admin\Requests\LoginAdminRequest;
use Sto\Modules\Admin\Tasks\LoginAdminTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class LoginAdminController.
 *
 * 
 */
class LoginAdminController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/admins/login",
     *      summary="Generate auth token for admin",
     *      tags={"Authentication Admin"},
     *      description="Generate authentication token based on email and password",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          default="{""email"":""admin@maued.dev"",""password"":""1234567""}",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="email",
     *                  description="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  description="password",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AdminSuccessResponse"
     *              ),
     *          )
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="Credentials Incorrect.",
     *     ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */
    /**
     * @param LoginAdminRequest $loginAdminRequest
     * @param LoginAdminTask $loginTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(LoginAdminRequest $loginAdminRequest, LoginAdminTask $loginAdminTask)
    {
        $user = $loginAdminTask->run($loginAdminRequest['email'], $loginAdminRequest['password']);
        return $this->response->item($user, new UserTransformer());
    }
}
