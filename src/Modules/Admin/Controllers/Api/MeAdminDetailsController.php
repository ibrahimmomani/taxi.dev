<?php

namespace Sto\Modules\Admin\Controllers\Api;

use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Admin\Tasks\MeAdminDetailsTask;
use Sto\Services\Core\Request\Manager\HttpRequest;
use Sto\Modules\User\Transformers\UserTransformer;

class MeAdminDetailsController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/admins/me",
     *      summary="Admin Details",
     *      tags={"Admin Account"},
     *      description="Get current admin details",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="OK.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="bad credentials.",
     *     )
     * )
     */

    /**
     * @param HttpRequest $httpRequest
     * @param MeAdminDetailsTask $me
     * @return \Dingo\Api\Http\Response
     */
    public function handle(HttpRequest $httpRequest, MeAdminDetailsTask $me)
    {
        $user = $me->run();
        return $this->response->item($user, new UserTransformer());
    }
}