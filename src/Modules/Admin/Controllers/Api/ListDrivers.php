<?php


namespace Sto\Modules\Admin\Controllers\Api;


use Sto\Modules\Admin\Tasks\ListDriversTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

class ListDrivers extends ApiController
{

    public function handle(ListDriversTask $listDriversTask)
    {
        $users = $listDriversTask->run();

        return $this->response->paginator($users, new UserTransformer());
    }

}