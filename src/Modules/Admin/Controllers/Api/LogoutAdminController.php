<?php

namespace Sto\Modules\Admin\Controllers\Api;

use Sto\Modules\Admin\Tasks\LogoutAdminTask;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Services\Core\Request\Manager\HttpRequest;

/**
 * Class LogoutAdminController.
 *
 * 
 */
class LogoutAdminController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/admins/me/logout",
     *      summary="Logout admin",
     *      tags={"Admin Account"},
     *      description="Logout admin and revoke jwt token.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="authorization",
     *          type="string",
     *          in="header",
     *          description=""
     *      ),
     *      @SWG\Response(
     *          response=202,
     *          description="Accepted.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="Revoked and blacklisted token.",
     *     )
     * )
     */
    /**
     * @param HttpRequest $request
     * @param LogoutAdminTask $logoutAdminTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(HttpRequest $request, LogoutAdminTask $logoutAdminTask)
    {
        $logoutAdminTask->run($request->header('authorization'));

        return $this->response->accepted(null, [
            'message' => 'Admin Logged Out Successfully.',
        ]);
    }
}
