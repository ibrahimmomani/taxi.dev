<?php

namespace Sto\Modules\Driver\Tasks;


use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Driver\Contracts\CarRepositoryInterface;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Driver\Requests\MeDriverUpdateCarInfoRequest;

class MeDriverUpdateCarInfoTask extends Task
{

    private $authenticationService;

    private $carRepository;

    private $userRepository;

    /**
     * MeDriverUpdateCarInfoTask constructor.
     * @param CarRepositoryInterface $carRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        CarRepositoryInterface $carRepository,
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->carRepository = $carRepository;
        $this->userRepository = $userRepository;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param MeDriverUpdateBasicInfoRequest $request
     * @return mixed
     */
    public function run(MeDriverUpdateCarInfoRequest $meDriverUpdateCarInfoRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $meDriverUpdateCarInfoRequest->except('token');
        $data['user_id'] = $user->id;
        if($this->carRepository->updateOrCreate(['user_id' => $user->id], $data)){
            return $this->userRepository->find($user->id);
        }
    }

}