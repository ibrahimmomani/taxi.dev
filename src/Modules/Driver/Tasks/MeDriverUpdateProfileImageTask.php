<?php

namespace Sto\Modules\Driver\Tasks;


use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Driver\Requests\MeDriverUpdateProfileImageRequest;
use Antennaio\Clyde\ClydeUpload;
use Antennaio\Clyde\Facades\ClydeImage;

class MeDriverUpdateProfileImageTask extends Task
{

    private $authenticationService;

    private $userRepository;

    protected $uploads;

    /**
     * MeDriverUpdateProfileImageTask constructor.
     * @param UserRepositoryInterface $userRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService,
        ClydeUpload $uploads
    ) {
        $this->userRepository = $userRepository;
        $this->authenticationService = $authenticationService;
        $this->uploads = $uploads;
    }

    /**
     * @param MeDriverUpdateProfileImageRequest $meDriverUpdateProfileImageRequest
     * @return mixed
     */
    public function run(MeDriverUpdateProfileImageRequest $meDriverUpdateProfileImageRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        if ($meDriverUpdateProfileImageRequest->hasFile('picture_url')) {
            $filename = $this->uploads->upload($meDriverUpdateProfileImageRequest->file('picture_url'));
            $data = ['picture_url' => ClydeImage::url($filename, ['p' => 'small'])];
            return $this->userRepository->update($data, $user->id);
        }
    }

}