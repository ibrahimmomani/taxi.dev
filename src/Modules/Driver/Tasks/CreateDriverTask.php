<?php

namespace Sto\Modules\Driver\Tasks;

use Exception;
use Illuminate\Support\Facades\Hash;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Driver\Exceptions\AccountFailedException;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class CreateDriverTask.
 *
 * 
 */
class CreateDriverTask extends Task
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * CreateDriverTask constructor.
     *
     * @param \Sto\Modules\User\Contracts\UserRepositoryInterface $userRepository
     * @param \Sto\Services\Authentication\Portals\AuthenticationService $authenticationService
     */
    public function __construct(UserRepositoryInterface $userRepository, AuthenticationService $authenticationService)
    {
        $this->userRepository = $userRepository;
        $this->authenticationService = $authenticationService;
    }

    /**
     * create a new user object.
     * optionally can login the created user and return it with its token.
     *
     * @param      $email
     * @param      $password
     * @param      $name
     * @param bool $login determine weather to login or not after creating
     *
     * @return mixed
     */
    public function run($email, $password, $name, $phone_number='+15915544326',$login = false)
    {
        $hashedPassword = Hash::make($password);

        try {
            // create new user
            $user = $this->userRepository->create([
                'email'    => $email,
                'password' => $hashedPassword,
                'name'     => $name,
                'phone_number'=> $phone_number,
            ]);
            $user->attachRole(3);
        } catch (Exception $e) {
            throw (new AccountFailedException())->debug($e);
        }

        if ($login) {
            // login this user using it's object and inject it's token on it
            $user = $this->authenticationService->loginFromObject($user);
        }

        return $user;
    }
}
