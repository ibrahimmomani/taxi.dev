<?php

namespace Sto\Modules\Driver\Tasks;

use Illuminate\Support\Facades\Hash;
use Sto\Modules\Driver\Exceptions\AccountUpdateException;
use Sto\Modules\Driver\Requests\MeDriverChangePasswordRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class MeDriverChangePasswordTask
 * @package Sto\Modules\Driver\Tasks
 */
class MeDriverChangePasswordTask extends Task
{
    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;
    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * MeDriverChangePasswordTask constructor.
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
    }

    /**
     * Change password.
     *
     * @param MeDriverChangePasswordRequest $request
     * @return mixed
     */
    public function run(MeDriverChangePasswordRequest $request)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        if(!Hash::check($request->password, $user->password))
        {
            throw new AccountUpdateException();
        }
        $hashedNewPassword = Hash::make($request->newPassword);

        if($this->userRepository->update(['password' => $hashedNewPassword],$user->id))
        {
            $token = $this->authenticationService->refreshToken();
            $user->injectToken($token);
            return $user;
        }
    }

}