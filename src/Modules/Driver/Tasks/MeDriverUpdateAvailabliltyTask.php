<?php

namespace Sto\Modules\Driver\Tasks;


use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Driver\Requests\MeDriverUpdateAvailabliltyRequest;

class MeDriverUpdateAvailabliltyTask extends Task
{

    private $authenticationService;

    private $userRepository;

    /**
     * MeDriverUpdateAvailabliltyTask constructor.
     * @param UserRepositoryInterface $userRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->userRepository = $userRepository;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param MeDriverUpdateAvailabliltyRequest $meDriverUpdateAvailabliltyRequest
     * @return mixed
     */
    public function run(MeDriverUpdateAvailabliltyRequest $meDriverUpdateAvailabliltyRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $meDriverUpdateAvailabliltyRequest->except('token');

        return $this->userRepository->update($data, $user->id);
    }

}