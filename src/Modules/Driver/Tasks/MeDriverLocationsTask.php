<?php

namespace Sto\Modules\Driver\Tasks;

use Sto\Modules\Driver\Requests\MeDriverLocationsRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class MeDriverLocationsTask
 * @package Sto\Modules\Driver\Tasks
 */
class MeDriverLocationsTask extends Task
{
    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;
    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * MeDriverChangePasswordTask constructor.
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param MeDriverLocationsRequest $request
     * @return mixed
     */
    public function run(MeDriverLocationsRequest $request)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        return $this->userRepository->update($request->all(),$user->id);
    }
}