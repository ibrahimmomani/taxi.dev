<?php

namespace Sto\Modules\Driver\Tasks;

use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Services\Core\Exception\Exceptions\AuthorizationFailedException;

/**
 * Class CreateDriverTask.
 *
 * 
 */
class LoginDriverTask extends Task
{

    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * LoginTask constructor.
     *
     * @param \Sto\Services\Authentication\Portals\AuthenticationService $authenticationService
     */
    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param $email
     * @param $password
     *
     * @return mixed
     */
    public function run($email, $password)
    {
        $token = $this->authenticationService->login($email, $password);

        $user = $this->authenticationService->getAuthenticatedUser($token);

        if(!$user->hasRole('driver')){
            throw new AuthorizationFailedException('Authorization Failed.');
        }

        return $user;
    }
}
