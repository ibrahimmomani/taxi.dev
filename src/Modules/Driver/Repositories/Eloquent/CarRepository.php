<?php

namespace Sto\Modules\Driver\Repositories\Eloquent;

use Sto\Modules\Driver\Contracts\CarRepositoryInterface;
use Sto\Modules\Driver\Models\Car;
use Sto\Services\Core\Repository\Abstracts\Repository;

/**
 * Class CarRepository
 * @package Sto\Modules\Driver\Repositories\Eloquent
 */
class CarRepository extends Repository implements CarRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'car_number'  => 'like',
        'car_model' => 'like',
    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Car::class;
    }
}
