<?php

namespace Sto\Modules\Driver\Controllers\Api;

use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Driver\Requests\MeDriverChangePasswordRequest;
use Sto\Modules\Driver\Tasks\MeDriverChangePasswordTask;

/**
 * Class MeDriverChangePasswordController
 * @package Sto\Modules\Driver\Controllers\Api
 */
class MeDriverChangePasswordController extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/drivers/me/settings/password",
     *      summary="Change Driver Password",
     *      tags={"Driver Account"},
     *      description="Change driver password and refresh token for user",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="password",
     *                  description="password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="newPassword",
     *                  description="New Password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="newPassword_confirmation",
     *                  description="New Password Confirmation",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */

    /**
     * @param MeDriverChangePasswordRequest $changePasswordRequest
     * @param MeDriverChangePasswordTask $changePasswordTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        MeDriverChangePasswordRequest $changePasswordRequest,
        MeDriverChangePasswordTask $changePasswordTask
    ) {
        $result = $changePasswordTask->run($changePasswordRequest);
        return $this->response->item($result, new UserTransformer());
    }
}