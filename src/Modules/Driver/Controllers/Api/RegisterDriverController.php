<?php

namespace Sto\Modules\Driver\Controllers\Api;

use Sto\Modules\Driver\Requests\RegisterDriverRequest;
use Sto\Modules\Driver\Tasks\CreateDriverTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class RegisterController.
 *
 * 
 */
class RegisterDriverController extends ApiController
{
        /**
     * @SWG\Post(
     *      path="/drivers/register",
     *      summary="Register a driver",
     *      tags={"Authentication Driver"},
     *      description="Register a driver and log him in by generating his token",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          default="{""name"":""John Doe"",""email"":""driver@maued.dev"",""password"":""1234567"",""phone_number"":""+9721231232""}",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="name",
     *                  description="name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="email",
     *                  description="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  description="password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="phone_number",
     *                  description="phone_number",
     *                  type="string"
     *              ),     
     *              @SWG\Property(
     *                  property="latitude",
     *                  description="latitude",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="longitude",
     *                  description="longitude",
     *                  type="string"
     *              ),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */

    /**
     * @param RegisterDriverRequest $registerRequest
     * @param CreateDriverTask $createDriverTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        RegisterDriverRequest $registerRequest,
        CreateDriverTask $createDriverTask
    ) {
        $user = $createDriverTask->run(
            $registerRequest['email'],
            $registerRequest['password'],
            $registerRequest['name'],
            $registerRequest['phone_number'],
            true
        );

        return $this->response->item($user, new UserTransformer());
    }
}
