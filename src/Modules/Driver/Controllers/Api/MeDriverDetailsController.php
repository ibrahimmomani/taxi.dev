<?php

namespace Sto\Modules\Driver\Controllers\Api;

use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Driver\Tasks\MeDriverDetailsTask;
use Sto\Services\Core\Request\Manager\HttpRequest;
use Sto\Modules\Driver\Transformers\DriverDetailsTransformer;

class MeDriverDetailsController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/drivers/me",
     *      summary="Driver Details",
     *      tags={"Driver Account"},
     *      description="Get current rider details",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="OK.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="bad credentials.",
     *     )
     * )
     */

    /**
     * @param HttpRequest $httpRequest
     * @param MeDriverDetailsTask $me
     * @return \Dingo\Api\Http\Response
     */
    public function handle(HttpRequest $httpRequest, MeDriverDetailsTask $me)
    {
        $user = $me->run();
        return $this->response->item($user, new DriverDetailsTransformer());
    }
}