<?php

namespace Sto\Modules\Driver\Controllers\Api;


use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Driver\Requests\MeDriverUpdateAvailabliltyRequest;
use Sto\Modules\Driver\Tasks\MeDriverUpdateAvailabliltyTask;
use Sto\Modules\Driver\Transformers\DriverTransformer;

class MeDriverUpdateAvailablilty extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/drivers/me/settings/availablilty",
     *      summary="Change availablilty",
     *      tags={"Driver Account"},
     *      description="Change driver availablilty",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="available",
     *                  description="available : true or false",
     *                  type="boolean"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverUpdateResponse"
     *              )
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */
    public function handle
    (
        MeDriverUpdateAvailabliltyRequest $meDriverUpdateAvailabliltyRequest,
        MeDriverUpdateAvailabliltyTask $meDriverUpdateAvailabliltyTask
    )
    {
        $result = $meDriverUpdateAvailabliltyTask->run($meDriverUpdateAvailabliltyRequest);
        return $this->response->item($result, new DriverTransformer());
    }

}