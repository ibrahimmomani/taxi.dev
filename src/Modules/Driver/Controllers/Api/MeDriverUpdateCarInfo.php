<?php

namespace Sto\Modules\Driver\Controllers\Api;


use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Driver\Requests\MeDriverUpdateCarInfoRequest;
use Sto\Modules\Driver\Tasks\MeDriverUpdateCarInfoTask;
use Sto\Modules\Driver\Transformers\DriverTransformer;

class MeDriverUpdateCarInfo extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/drivers/me/settings/cars",
     *      summary="Change Driver Car Info",
     *      tags={"Driver Account"},
     *      description="Change driver car information.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="car_type_id",
     *                  description="Car type Id [1, 2, 3]",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="car_number",
     *                  description="car_number",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="car_model",
     *                  description="car_model",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="car_year",
     *                  description="car_year",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverUpdateResponse"
     *              )
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */
    public function handle
    (
        MeDriverUpdateCarInfoRequest $meDriverUpdateCarInfoRequest,
        MeDriverUpdateCarInfoTask $meDriverUpdateCarInfoTask
    )
    {
        $result = $meDriverUpdateCarInfoTask->run($meDriverUpdateCarInfoRequest);
        return $this->response->item($result, new DriverTransformer());
    }

}