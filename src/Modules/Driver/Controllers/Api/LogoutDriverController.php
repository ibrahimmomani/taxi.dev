<?php

namespace Sto\Modules\Driver\Controllers\Api;

use Sto\Modules\Driver\Tasks\LogoutDriverTask;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Services\Core\Request\Manager\HttpRequest;

/**
 * Class LogoutController.
 *
 * 
 */
class LogoutDriverController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/drivers/me/logout",
     *      summary="Logout driver",
     *      tags={"Driver Account"},
     *      description="Logout driver and revoke jwt token.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="authorization",
     *          type="string",
     *          in="header",
     *          description=""
     *      ),
     *      @SWG\Response(
     *          response=202,
     *          description="Accepted.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="Revoked and blacklisted token.",
     *     )
     * )
     */
    /**
     * @param HttpRequest $request
     * @param LogoutDriverTask $logoutDriverTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(HttpRequest $request, LogoutDriverTask $logoutDriverTask)
    {
        $logoutDriverTask->run($request->header('authorization'));

        return $this->response->accepted(null, [
            'message' => 'Driver Logged Out Successfully.',
        ]);
    }
}
