<?php

namespace Sto\Modules\Driver\Controllers\Api;

use Sto\Modules\Driver\Transformers\DriverTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Driver\Requests\MeDriverUpdateBasicInfoRequest;
use Sto\Modules\Driver\Tasks\MeDriverUpdateBasicInfoTask;
/**
 * Class MeDriverUpdateBasicInfoController
 * @package Sto\Modules\Driver\Controllers\Api
 */
class MeDriverUpdateBasicInfoController extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/drivers/me/settings/basic",
     *      summary="Change Driver Basic Info",
     *      tags={"Driver Account"},
     *      description="Change driver basic information such as name and locale",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="name",
     *                  description="name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="locale",
     *                  description="locale",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverUpdateResponse"
     *              )
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */
    /**
     * @param MeDriverUpdateBasicInfoRequest $basicInfoRequest
     * @param MeDriverUpdateBasicInfoTask $basicInfoTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        MeDriverUpdateBasicInfoRequest $basicInfoRequest,
        MeDriverUpdateBasicInfoTask $basicInfoTask
    )
    {
        $result = $basicInfoTask->run($basicInfoRequest);
        return $this->response->item($result, new DriverTransformer());
    }
}