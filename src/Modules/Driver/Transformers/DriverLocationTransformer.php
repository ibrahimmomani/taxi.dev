<?php

namespace Sto\Modules\Driver\Transformers;

use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class DriverLocationTransformer
 * @package Sto\Modules\Driver\Transformers
 */
class DriverLocationTransformer extends Transformer
{

    /**
     * @param \Sto\Modules\User\Models\User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'user_id' => (int) $user->id,
            'location'=>
                [
                'latitude' => $user->latitude,
                'longitude' => $user->longitude
                ]
        ];
    }
}