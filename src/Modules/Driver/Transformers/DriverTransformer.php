<?php

namespace Sto\Modules\Driver\Transformers;

use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class DriverTransformer
 * @package Sto\Modules\Driver\Transformers
 */
class DriverTransformer extends Transformer
{

    /**
     * @param \Sto\Modules\User\Models\User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'user_id' => (int) $user->id,
            'name' => $user->name,
            'locale' => $user->locale,
            'picture_url'=>$user->picture_url,
            'available' => $user->available,
            'car' => [
                'car_type' => ($user->cars->first()) ? $user->cars->first()->type->type : null,
                'car_number' => ($user->cars->first()) ? $user->cars->first()->car_number : null,
                'car_model' => ($user->cars->first()) ? $user->cars->first()->car_model : null,
                'car_year' => ($user->cars->first()) ? $user->cars->first()->car_year : null,
            ],
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ];
    }
}
