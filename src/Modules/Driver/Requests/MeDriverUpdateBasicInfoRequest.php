<?php

namespace Sto\Modules\Driver\Requests;

use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class MeDriverUpdateBasicInfoRequest
 * @package Sto\Modules\Driver\Requests
 */
class MeDriverUpdateBasicInfoRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:6|max:50|filled',
            'locale'     => 'min:2|max:2|filled'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}