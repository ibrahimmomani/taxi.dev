<?php

namespace Sto\Modules\Driver\Requests;

use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class MeDriverChangePasswordRequest
 * @package Sto\Modules\Driver\Requests
 */
class MeDriverChangePasswordRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:6|max:50',
            'newPassword'     => 'required|min:6|max:50|confirmed',
            'newPassword_confirmation' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}