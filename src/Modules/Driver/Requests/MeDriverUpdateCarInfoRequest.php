<?php


namespace Sto\Modules\Driver\Requests;


use Sto\Services\Core\Request\Abstracts\Request;

class MeDriverUpdateCarInfoRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_type_id' => 'filled|exists:car_types,id',
            'car_number'  => 'filled|min:4',
            'car_model'   =>'filled|min:4',
            'car_year'    => 'filled|min:3',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}