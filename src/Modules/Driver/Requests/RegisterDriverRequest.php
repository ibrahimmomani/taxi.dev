<?php

namespace Sto\Modules\Driver\Requests;

use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class RegisterRequest.
 *
 * 
 */
class RegisterDriverRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email|max:40|unique:users',
            'password' => 'required|min:6|max:30',
            'name'     => 'required|min:2|max:50',
            'phone_number' => 'required|unique:users',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
