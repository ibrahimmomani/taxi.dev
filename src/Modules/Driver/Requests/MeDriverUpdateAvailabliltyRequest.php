<?php


namespace Sto\Modules\Driver\Requests;


use Sto\Services\Core\Request\Abstracts\Request;

class MeDriverUpdateAvailabliltyRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'available'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}