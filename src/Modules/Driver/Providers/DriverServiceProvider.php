<?php

namespace Sto\Modules\Driver\Providers;

use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Driver\Contracts\CarRepositoryInterface;
use Sto\Modules\User\Repositories\Eloquent\UserRepository;
use Sto\Modules\Driver\Repositories\Eloquent\CarRepository;
use Sto\Services\Core\Providers\Abstracts\ServiceProvider;

/**
 * Class DriverServiceProvider.
 *
 * 
 */
class DriverServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Module Service providers to be registered.
     *
     * @var array
     */
    protected $providers = [
        RoutesServiceProvider::class,
        AuthServiceProvider::class,
    ];

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->registerServiceProviders($this->providers);
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        $this->registerTheDatabaseMigrationsFiles(__DIR__);

        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(CarRepositoryInterface::class, CarRepository::class);
    }
}
