<?php


namespace Sto\Modules\Ride\Requests\Driver;


use Sto\Services\Core\Request\Abstracts\Request;

class GetActiveRideRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}