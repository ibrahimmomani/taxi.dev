<?php

namespace Sto\Modules\Ride\Requests\Driver;


use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class GetRideRequest
 * @package Sto\Modules\Ride\Requests\Driver
 */
class GetRideRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' =>  'required|exists:rides,uuid|regex:/^(\{)?[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}(?(1)\})$/i',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}