<?php


namespace Sto\Modules\Ride\Requests\Rider;


use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class GetCarsRequest
 * @package Sto\Modules\Ride\Requests\Rider
 */
class GetCarsRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}