<?php

namespace Sto\Modules\Ride\Requests\Rider;


use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class PostRideRequest
 * @package Sto\Modules\Ride\Requests\Rider
 */
class PostRideRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude_from' => ['filled', 'regex:/^([-+]?\d{1,2}([.]\d+)?)$/'],
            'longitude_from' => ['filled', 'regex:/\s*([-+]?\d{1,3}([.]\d+)?)$/'],
            'from_txt' => 'filled|min:3',
            'latitude_to' => ['filled', 'regex:/^([-+]?\d{1,2}([.]\d+)?)$/'],
            'longitude_to' => ['filled', 'regex:/\s*([-+]?\d{1,3}([.]\d+)?)$/'],
            'to_txt' => 'filled|min:3',
            'pickup_time' => 'filled|date_format:Y-m-d H:i:s',
            'people' => 'filled|min:1|max:4'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}