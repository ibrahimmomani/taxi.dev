<?php

namespace Sto\Modules\Ride\Requests\Rider;


use Sto\Services\Core\Request\Abstracts\Request;

class GetNearByRideRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude' => ['filled', 'regex:/^([-+]?\d{1,2}([.]\d+)?)$/'],
            'longitude' => ['filled', 'regex:/\s*([-+]?\d{1,3}([.]\d+)?)$/'],
            'radius' => ['filled', 'integer', 'min:1', 'max:10']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}