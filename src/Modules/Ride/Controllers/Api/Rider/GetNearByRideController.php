<?php

namespace Sto\Modules\Ride\Controllers\Api\Rider;


use Sto\Modules\Ride\Tasks\Rider\GetNearByRideTask;
use Sto\Modules\Ride\Requests\Rider\GetNearByRideRequest;
use Sto\Modules\User\Transformers\UserNearByTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Log;

class GetNearByRideController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/me/nearby",
     *      summary="Get NearBy Drivers",
     *      tags={"Rider Rides"},
     *      description="Get NearBy Drivers.",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *         description="latitude",
     *         name="latitude",
     *         in="query",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="longitude",
     *         name="longitude",
     *         in="query",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="radius",
     *         name="radius",
     *         in="query",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideNearBySuccessResponse"
     *              ),
     *          )
     *      )
     * )
     */
    public function handle
    (
        GetNearByRideRequest $getNearByRideRequest,
        GetNearByRideTask $getNearByRideTask
    )
    {

        $result = $getNearByRideTask->run($getNearByRideRequest);
        return $this->response->paginator($result, new UserNearByTransformer());
    }
}