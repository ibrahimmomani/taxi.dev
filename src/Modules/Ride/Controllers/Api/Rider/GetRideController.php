<?php

namespace Sto\Modules\Ride\Controllers\Api\Rider;

use Sto\Modules\Ride\Exceptions\RideRetrieveException;
use Sto\Modules\Ride\Transformers\RideDetailsTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\GetRideRequest;
use Sto\Modules\Ride\Tasks\Rider\GetRideTask;
use Log;
/**
 * Class GetRideController
 * @package Sto\Modules\Ride\Controllers\Api\Rider
 */
class GetRideController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides",
     *      summary="Get ride by uuid",
     *      tags={"Rider Rides"},
     *      description="Get My Ride Requests. <br> http://url.dev/api/v1/rides?uuid={uuid}",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *         description="uuid of request",
     *         name="uuid",
     *         in="query",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideDetailsResponse"
     *              ),
     *          )
     *      )
     * )
     */

    /**
     * @param GetRideRequest $getRideRequest
     * @param GetRideTask $getRideTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        GetRideRequest $getRideRequest,
        GetRideTask $getRideTask
    )
    {

        try {
            $result = $getRideTask->run($getRideRequest);
            return $this->response->item($result, new RideDetailsTransformer());
        } catch (RideRetrieveException $e) {
            Log::error($e->getMessage());
        }
    }
}