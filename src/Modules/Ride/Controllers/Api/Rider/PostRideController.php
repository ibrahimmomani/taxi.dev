<?php

namespace Sto\Modules\Ride\Controllers\Api\Rider;


use Sto\Modules\Ride\Exceptions\RideFailedException;
use Sto\Modules\Ride\Transformers\RideCreatedTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\PostRideRequest;
use Sto\Modules\Ride\Tasks\Rider\PostRideTask;  
use Log;

/**
 * Class PostRideController
 * @package Sto\Modules\Ride\Controllers\Api\Rider
 */
class PostRideController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/rides/me",
     *      summary="Request a ride",
     *      tags={"Rider Rides"},
     *      description="Issue a ride request.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ride that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Ride"
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideSuccessPostedResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */

    /**
     * @param PostRideRequest $postRideRequest
     * @param PostRideTask $postRideTask
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(
        PostRideRequest $postRideRequest,
        PostRideTask $postRideTask
    )
    {
        try {
            $result = $postRideTask->run($postRideRequest);
            return $this->response->item($result, new RideCreatedTransformer());
        } catch (RideFailedException $e) {
            Log::error($e->getMessage());
        }
    }
}