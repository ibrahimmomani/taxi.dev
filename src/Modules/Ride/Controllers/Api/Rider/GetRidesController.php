<?php

namespace Sto\Modules\Ride\Controllers\Api\Rider;

use Sto\Modules\Ride\Exceptions\RideRetrieveException;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\GetRidesRequest;
use Sto\Modules\Ride\Tasks\Rider\GetRidesTask;
use Sto\Modules\Ride\Transformers\RideTransformer;
use Log;

/**
 * Class GetRidesController
 * @package Sto\Modules\Ride\Controllers\Api\Rider
 */
class GetRidesController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/me",
     *      summary="Get my ride requests.",
     *      tags={"Rider Rides"},
     *      description="Get My Ride Requests.",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideSuccessResponse"
     *              ),
     *          )
     *      )
     * )
     */

    /**
     * @param GetRideRequest $postRideRequest
     * @param GetRideTask $postRideTask
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(
        GetRidesRequest $getRidesRequest,
        GetRidesTask $getRidesTask
    )
    {
        try {
            $result = $getRidesTask->run();
            return $this->response->paginator($result, new RideTransformer());
        } catch (RideRetrieveException $e) {
            Log::error($e->getMessage());
        }
    }
}