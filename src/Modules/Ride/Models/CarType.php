<?php

namespace Sto\Modules\Ride\Models;


use Sto\Modules\Driver\Models\Car;
use Sto\Services\Core\Model\Abstracts\Model;

class CarType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'car_types';
    protected $fillable = ['type'];
    
    public function rides()
    {
        return $this->belongsTo(Ride::class);
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'car_type_id');
    }
}