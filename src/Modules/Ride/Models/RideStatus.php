<?php

namespace Sto\Modules\Ride\Models;

use Sto\Services\Core\Model\Abstracts\Model;

class RideStatus extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ride_statuses';

    protected $fillable = ['status'];
    
    public function rides()
    {
        return $this->hasMany(\Sto\Modules\Ride\Models\Ride::class);
    }
    public function driverRides()
    {
        return $this->hasMany(\Sto\Modules\Ride\Models\DriverRide::class);
    }
    
}