<?php

namespace Sto\Modules\Ride\Models;

use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Services\Core\Model\Abstracts\Model;
use Sto\Services\Uuid\Traits\Uuids;

/**
 * Class Ride.
 *
 * 
 */
class Ride extends Model

{
    use Uuids;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rides';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The Table primary key
     *
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'latitude_from',
        'longitude_from',
        'from_txt',
        'latitude_to',
        'longitude_to',
        'to_txt',
        'fare',
        'pickup_time',
        'people',
        'ride_status_id',
        'car_type_id'
    ];

    /**
     * The dates attributes.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];


    public function rideStatus()
    {
        return $this->belongsTo(RideStatus::class);
    }

    public function user()
    {
        return $this->belongsTo(\Sto\Modules\User\Models\User::class);
    }

    public function accepted()
    {
        return $this->hasOne(DriverRide::class, 'uuid');
    }
}
