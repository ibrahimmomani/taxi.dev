<?php

namespace Sto\Modules\Ride\Models;

use Sto\Modules\User\Models\User;
use Sto\Services\Core\Model\Abstracts\Model;

/**
 * Class DriverRide
 * @package Sto\Modules\Ride\Models
 */
class DriverRide extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'driver_rides';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'user_id',
        'ride_status_id',
    ];

    /**
     * The dates attributes.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ride()
    {
        return $this->belongsTo(Ride::class, 'uuid');
    }
    public function rideStatus()
    {
        return $this->belongsTo(RideStatus::class);
    }
}