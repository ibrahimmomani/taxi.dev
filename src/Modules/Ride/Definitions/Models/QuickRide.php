<?php


namespace Sto\Modules\Ride\Definitions\Models;

/**
 * @SWG\Definition(
 *      definition="QuickRide",
 *      @SWG\Property(
 *          property="latitude_from",
 *          description="Latitude From",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitude_from",
 *          description="Longitude From",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="from_txt",
 *          description="from_txt",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="driver_id",
 *          description="driver_id",
 *          type="integer"
 *      )
 *
 * )
 *
 */

class QuickRide
{

}