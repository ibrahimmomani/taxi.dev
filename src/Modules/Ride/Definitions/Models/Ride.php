<?php

namespace Sto\Modules\Ride\Definitions\Models;

/**
 * @SWG\Definition(
 *      definition="Ride",
 *      @SWG\Property(
 *          property="latitude_from",
 *          description="Latitude From",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitude_from",
 *          description="Longitude From",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="from_txt",
 *          description="From Text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude_to",
 *          description="Latitude To",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitude_to",
 *          description="Longitude To",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="to_txt",
 *          description="To Text",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="pickup_time",
 *          description="pickup_time",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="people",
 *          description="people [1, 2, 3 , 4]",
 *          type="integer"
 *      )
 * )
 */
class Ride
{
}