<?php
namespace Sto\Modules\Ride\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="RideSuccessResponse",
 *      @SWG\Property(
 *          property="uuid",
 *          description="uuid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rider",
 *          description="rider",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="driver",
 *          description="driver",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="object"
 *      ),
 * )
 */

class RideSuccessResponse
{
}