<?php

namespace Sto\Modules\Ride\Providers;

use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Modules\Ride\Repositories\Eloquent\RideRepository;
use Sto\Modules\Ride\Repositories\Eloquent\DriverRideRepository;
use Sto\Services\Core\Providers\Abstracts\ServiceProvider;

/**
 * Class RideServiceProvider.
 *
 * 
 */
class RideServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Module Service providers to be registered.
     *
     * @var array
     */
    protected $providers = [
        RoutesServiceProvider::class,
        AuthServiceProvider::class,
    ];

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->registerServiceProviders($this->providers);
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        $this->registerTheDatabaseMigrationsFiles(__DIR__);

        $this->app->bind(RideRepositoryInterface::class, RideRepository::class);
        $this->app->bind(DriverRideRepositoryInterface::class, DriverRideRepository::class);
    }
}
