<?php

namespace Sto\Modules\Ride\Transformers;

use Sto\Modules\Ride\Models\Ride;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class RideCreatedTransformer
 * @package Sto\Modules\Ride\Transformers
 */
class RideCreatedTransformer extends Transformer
{
    /**
     * @param Ride $ride
     * @return array
     */
    public function transform(Ride $ride)
    {
        //dd($ride);
        return [
            'uuid'       => $ride->uuid,
            'fare'       => $ride->fare,
            'eta'        => ($ride->latitude_to && $ride->longitude_to) ?
                \Sto\Services\GeoTools\GeoTools::getEta(
                    $ride->latitude_from,
                    $ride->longitude_from,
                    $ride->latitude_to,
                    $ride->longitude_to
                ) : null,
            'created_at' => $ride->created_at,
            'updated_at' => $ride->updated_at,
        ];
    }

}
