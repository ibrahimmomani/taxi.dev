<?php

namespace Sto\Modules\Ride\Seeders;

use Illuminate\Database\Seeder;
use Sto\Modules\Ride\Models\CarType;
use Sto\Modules\Ride\Models\RideStatus;

class SeedRideStatusesAndCarTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $car1 = new CarType();
        $car1->type= "Taxi";
        $car1->save();

        $car2 = new CarType();
        $car2->type= "Van";
        $car2->save();

        $car3 = new CarType();
        $car3->type= "Car";
        $car3->save();



        $s1 = new RideStatus();
        $s1->status= "STARTED";
        $s1->save();

        $s2 = new RideStatus();
        $s2->status= "PROCESSING";
        $s2->save();

        $s3 = new RideStatus();
        $s3->status= "CANCELED";
        $s3->save();


        $s4 = new RideStatus();
        $s4->status= "ACCEPTED";
        $s4->save();

        $s4 = new RideStatus();
        $s4->status= "REJECTED";
        $s4->save();

        $s6 = new RideStatus();
        $s6->status= "IN_PROGRESS";
        $s6->save();

        $s7 = new RideStatus();
        $s7->status= "COMPLETED";
        $s7->save();
    }
}