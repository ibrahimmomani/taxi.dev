<?php

namespace Sto\Modules\Ride\Tasks\Rider;


use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Modules\Ride\Requests\Rider\PutRideStatusRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;

class PutRideStatusTask extends Task
{
    private $authenticationService;
    private $rideRepository;
    private $driverRideRepository;

    /**
     * PutRideStatusTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param AuthenticationService $authenticationService
     * @param DriverRideRepositoryInterface $driverRideRepository
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        AuthenticationService $authenticationService,
        DriverRideRepositoryInterface $driverRideRepository
    ) {
        $this->authenticationService = $authenticationService;
        $this->rideRepository = $rideRepository;
        $this->driverRideRepository = $driverRideRepository;
    }

    /**
     * @param PutRideStatusRequest $putRideStatusRequest
     * @return mixed
     */
    public function run(PutRideStatusRequest $putRideStatusRequest)
    {
        $found  = $this->driverRideRepository->findByField('uuid', $putRideStatusRequest['uuid']);

        if ((boolean)$found === true){
            $this->driverRideRepository->update(['ride_status_id' => $putRideStatusRequest['ride_status_id']], $found->first()->id);
        }

        return $this
            ->rideRepository
            ->update(
                $putRideStatusRequest->all(),
                $putRideStatusRequest['uuid']
            );

    }

}