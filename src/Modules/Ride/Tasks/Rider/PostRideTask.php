<?php

namespace Sto\Modules\Ride\Tasks\Rider;

use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Requests\Rider\PostRideRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Fare\Fare;
use Sto\Services\GeoTools\GeoTools;

/**
 * Class PostRideTask
 * @package Sto\Modules\Ride\Tasks\Rider
 */
class PostRideTask extends Task
{
    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var \Sto\Modules\Ride\Contracts\RideRepositoryInterface
     */
    private $rideRepository;

    /**
     * PostUserRideTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param UserRepositoryInterface $userRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
        $this->rideRepository = $rideRepository;
    }

    /**
     * @param PostRideRequest $postRideRequest
     * @return mixed
     */
    public function run(PostRideRequest $postRideRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $postRideRequest->all();
        
        $data['user_id'] = $user->id;
        $data['ride_status_id'] = 1;
        $data['fare'] = Fare::fareCalculator(
                GeoTools::getDistance(
                    $postRideRequest->latitude_from,
                    $postRideRequest->longitude_from,
                    $postRideRequest->latitude_to,
                    $postRideRequest->longitude_to
                    ), 0.25);

        return $this->rideRepository->create($data);
    }

}