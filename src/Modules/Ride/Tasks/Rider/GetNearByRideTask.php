<?php

namespace Sto\Modules\Ride\Tasks\Rider;

use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Ride\Requests\Rider\GetNearByRideRequest;
use Sto\Modules\User\Repositories\Criterias\Eloquent\DriverNearByCriteria;

class GetNearByRideTask extends Task
{
    private $userRepository;
    private $authenticationService;

    /**
     * GetNearByRideTask constructor.
     * @param UserRepositoryInterface $userRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->userRepository = $userRepository;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param GetNearByRideRequest $getNearByRideRequest
     * @return mixed
     */
    public function run(GetNearByRideRequest $getNearByRideRequest)
    {
        $latitude = $getNearByRideRequest->latitude;
        $longitude = $getNearByRideRequest->longitude;
        $radius = $getNearByRideRequest->radius;

        $this->userRepository->pushCriteria(new DriverNearByCriteria($latitude, $longitude, $radius, 'km'));
        $nearByDriver = $this->userRepository->paginate();

        return $nearByDriver;
    }
}