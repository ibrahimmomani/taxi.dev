<?php


namespace Sto\Modules\Ride\Tasks\Rider;

use Sto\Modules\Ride\Contracts\CarTypeRepositoryInterface;
use Sto\Modules\Ride\Requests\Rider\GetCarsRequest;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class GetCarsTask
 * @package Sto\Modules\Ride\Tasks\Rider
 */
class GetCarsTask extends Task
{

    private $carTypeRepository;

    /**
     * GetCarsTask constructor.
     * @param CarTypeRepositoryInterface $carTypeRepository
     */
    public function __construct(
        CarTypeRepositoryInterface $carTypeRepository
    ) {
        $this->carTypeRepository = $carTypeRepository;
    }

    /**
     * @param GetCarsRequest $getCarsRequest
     * @return mixed
     */
    public function run(GetCarsRequest $getCarsRequest)
    {
        return $this->carTypeRepository->paginate();
    }
}