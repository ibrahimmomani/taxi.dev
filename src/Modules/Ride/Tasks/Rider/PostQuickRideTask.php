<?php

namespace Sto\Modules\Ride\Tasks\Rider;

use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Modules\Ride\Models\DriverRide;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Requests\Rider\PostQuickRideRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Fare\Fare;
use Sto\Services\GeoTools\GeoTools;

/**
 * Class PostQuickRideTask
 * @package Sto\Modules\Ride\Tasks\Rider
 */
class PostQuickRideTask extends Task
{

    private $authenticationService;

    private $userRepository;

    private $rideRepository;

    private $driverRideRepository;




    public function __construct(
        RideRepositoryInterface $rideRepository,
        UserRepositoryInterface $userRepository,
        DriverRideRepositoryInterface $driverRideRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
        $this->rideRepository = $rideRepository;
        $this->driverRideRepository = $driverRideRepository;
    }

    /**
     * @param PostQuickRideRequest $postRideRequest
     * @return mixed
     */
    public function run(PostQuickRideRequest $postRideRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $postRideRequest->all();
        
        $data['user_id'] = $user->id;
        $data['ride_status_id'] = 2;


        $ride = $this->rideRepository->create($data);
        if ($ride)
        {   $driverData['uuid'] = $ride->uuid;
            $driverData['user_id'] = $postRideRequest->driver_id;
            $driverData['ride_status_id'] = $ride->ride_status_id;

            if($this->driverRideRepository->updateOrCreate(
                [
                    'uuid' => $driverData['uuid'],
                    'user_id' => $driverData['user_id']
                ],
                $driverData
            )){
                return $ride;
            }
        }

    }

}