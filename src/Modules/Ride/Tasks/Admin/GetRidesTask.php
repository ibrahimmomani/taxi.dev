<?php


namespace Sto\Modules\Ride\Tasks\Admin;


use Sto\Modules\Ride\Requests\Admin\GetRidesRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\RideStatusCriteria;

class GetRidesTask extends Task
{
    private $rideRepository;

    /**
     * GetRidesTask constructor.
     * @param RideRepositoryInterface $rideRepository
     */
    public function __construct(
        RideRepositoryInterface $rideRepository
    ) {
        $this->rideRepository = $rideRepository;
    }

    /**
     * @param GetRidesRequest $getRidesCountsByStatusRequest
     * @return mixed
     */
    public function run(GetRidesRequest $getRidesCountsByStatusRequest)
    {
        $this->rideRepository->pushCriteria(new RideStatusCriteria($getRidesCountsByStatusRequest->status));

        return $this->rideRepository->paginate();
    }
}