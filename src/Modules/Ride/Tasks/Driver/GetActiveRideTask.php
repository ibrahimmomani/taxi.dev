<?php


namespace Sto\Modules\Ride\Tasks\Driver;


use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\ActiveRideCriteria;
use Sto\Modules\Ride\Requests\Driver\GetActiveRideRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;

class   GetActiveRideTask extends Task
{
    private $rideRepository;
    private $driverRideRepository;
    private $authenticationService;

    /**
     * GetActiveRideTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param AuthenticationService $authenticationService
     * @param DriverRideRepositoryInterface $driverRideRepository
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        AuthenticationService $authenticationService,
        DriverRideRepositoryInterface $driverRideRepository
    ) {
        $this->rideRepository = $rideRepository;
        $this->authenticationService = $authenticationService;
        $this->driverRideRepository = $driverRideRepository;
    }

    /**
     * @param GetActiveRideRequest $getActiveRideRequest
     * @return mixed
     */
    public function run(GetActiveRideRequest $getActiveRideRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $this->rideRepository->pushCriteria(new ActiveRideCriteria($user->id));
        $rides = $this->rideRepository->all()->last();


        return $rides;
    }
}