<?php

namespace Sto\Modules\Ride\Tasks\Driver;


use Sto\Modules\Ride\Requests\Driver\GetRideRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;

/**
 * Class GetRideTask
 * @package Sto\Modules\Ride\Tasks\Driver
 */
class GetRideTask extends Task
{
    private $rideRepository;

    /**
     * GetRideTask constructor.
     * @param RideRepositoryInterface $rideRepository
     */
    public function __construct(
        RideRepositoryInterface $rideRepository
    ) {
        $this->rideRepository = $rideRepository;
    }

    /**
     * @param GetRideRequest $getRideRequest
     * @return mixed
     */
    public function run(GetRideRequest $getRideRequest)
    {

        return $this->rideRepository->find($getRideRequest->uuid);
    }

}