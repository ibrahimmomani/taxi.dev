<?php

namespace Sto\Modules\Ride\Tasks\Driver;


use Sto\Modules\Ride\Repositories\Criterias\Eloquent\WhereUuidAndUserId;
use Sto\Modules\Ride\Requests\Driver\PutRideStatusRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;

class PutRideStatusTask extends Task
{

    private $authenticationService;

    private $rideRepository;

    private $driverRideRepository;

    /**
     * PutRideStatusTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param DriverRideRepositoryInterface $driverRideRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        DriverRideRepositoryInterface $driverRideRepository,
        AuthenticationService $authenticationService
    )
    {
        $this->authenticationService = $authenticationService;
        $this->rideRepository = $rideRepository;
        $this->driverRideRepository = $driverRideRepository;
    }

    /**
     * @param PutRideStatusRequest $putRideStatusRequest
     * @return mixed
     */
    public function run(PutRideStatusRequest $putRideStatusRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $putRideStatusRequest->all();
        $data['user_id'] = $user->id;
        if($data['ride_status_id'] !== 5){
            if ($this->rideRepository->update(['ride_status_id' => $data['ride_status_id']], $data['uuid'])) {
                return $this
                    ->driverRideRepository
                    ->updateOrCreate(
                        [
                            'uuid' => $data['uuid'],
                            'user_id' => $data['user_id'],

                        ],
                        [
                            'ride_status_id' => $data['ride_status_id']
                        ]
                    );
            }
        }else{
            $this->driverRideRepository->pushCriteria(new WhereUuidAndUserId($user->id, $putRideStatusRequest->uuid));
            $ride = $this->driverRideRepository->all()->first();

            if(isset($ride->id)){
                $this->rideRepository->update(['ride_status_id' => $data['ride_status_id']], $data['uuid']);
                $this->driverRideRepository->delete($ride->id);
                return null;
            }


            return $ride;
        }
       

    }

}