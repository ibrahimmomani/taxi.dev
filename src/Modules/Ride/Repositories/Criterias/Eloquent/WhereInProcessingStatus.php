<?php

namespace Sto\Modules\Ride\Repositories\Criterias\Eloquent;

use Sto\Services\Core\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class WhereInProcessingStatus extends Criteria
{

    /**
     * @param                                                   $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('ride_statuses_id', '=', 2);
    }

}