<?php

namespace Sto\Modules\Ride\Repositories\Criterias\Eloquent;

use Sto\Services\Core\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class WhereUuidInDriverRide extends Criteria
{
    private $uuid;

    /**
     * WhereUuidAndUserId constructor.
     * @param $userId
     * @param $uuid
     */
    public function __construct($uuid)
    {
        $this->uuid = $uuid;
    }


    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->whereRaw("uuid = ?",[$this->uuid]);
    }

}