<?php

namespace Sto\Modules\Ride\Repositories\Eloquent;

use Sto\Modules\Ride\Contracts\CarTypeRepositoryInterface;
use Sto\Modules\Ride\Models\CarType;
use Sto\Services\Core\Repository\Abstracts\Repository;

/**
 * Class CarTypeRepository
 * @package Sto\Modules\Ride\Repositories\Eloquent
 */
class CarTypeRepository extends Repository implements CarTypeRepositoryInterface
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type'  => '=',
        'id'  => '=',
    ];


    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return CarType::class;
    }

}