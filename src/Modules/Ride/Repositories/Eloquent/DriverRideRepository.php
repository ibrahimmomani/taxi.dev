<?php

namespace Sto\Modules\Ride\Repositories\Eloquent;

use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Modules\Ride\Models\DriverRide;
use Sto\Services\Core\Repository\Abstracts\Repository;

/**
 * Class DriverRideRepository
 * @package Sto\Modules\Ride\Repositories\Eloquent
 */
class DriverRideRepository extends Repository implements DriverRideRepositoryInterface
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id'  => '=',
        'uuid'  => '=',
    ];


    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return DriverRide::class;
    }

}