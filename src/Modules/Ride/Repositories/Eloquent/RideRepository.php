<?php

namespace Sto\Modules\Ride\Repositories\Eloquent;

use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Modules\Ride\Models\Ride;
use Sto\Services\Core\Repository\Abstracts\Repository;

class RideRepository extends Repository implements RideRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'uuid'  => '=',
    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Ride::class;
    }
}
