<?php
/**
 * Ride Module Routes File.
 */
$router->get('/ping', function () {
    return response()->json(['status' => 'Online.']);
});

$router->get('test', function(){
    return \Sto\Modules\Ride\Models\Ride::find('42f22940-24a4-11e6-b0f2-2f2b28f07ae9')->accepted->user->name;
});
$router->get('test2', function(){
    return \Sto\Modules\Ride\Models\DriverRide::find('9')->user->name;
});
$router->get('test3', function(){
    $v = Sto\Services\GeoTools\GeoTools::getDistance('32.05371890', '35.89969350', '32.037704', '35.864493');
    $fare = \Sto\Services\Fare\Fare::fareCalculator($v, 0.25);
    return \Sto\Services\GeoTools\GeoTools::getEta('32.05371890', '35.89969350', '32.037704', '35.864493');
    //echo "distance : ", $v, PHP_EOL ;
    //echo "time : ", $time, PHP_EOL ;
});
/**
 * Rider Ride Routes
 */
$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:rider'
            ]
    ], function ($router) {

    $router->post('me', ['uses' => 'Rider\PostRideController@handle']);
    $router->post('/me/quick', ['uses' => 'Rider\PostQuickRideController@handle']);
    $router->get('me', ['uses' => 'Rider\GetRidesController@handle']);
    $router->put('me/statuses', ['uses' => 'Rider\PutRideStatusController@handle']);
    $router->get('/', ['uses' => 'Rider\GetRideController@handle']);
    $router->get('me/statuses/', ['uses' => 'Rider\GetRideByStatusController@handle']);
    $router->get('/me/nearby', ['uses' => 'Rider\GetNearByRideController@handle']);
    $router->get('/cars', ['uses' => 'Rider\GetCarsController@handle']);


    
});

/**
 * Driver Ride Routes
 */
$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:driver'
            ]
    ], function ($router) {

    $router->get('/me/counts/statuses', ['uses' => 'Driver\GetRidesCountsByStatusController@handle']);
    $router->put('/drivers/statuses', ['uses' => 'Driver\PutRideStatusController@handle']);
    $router->get('/r/', ['uses' => 'Driver\GetRideController@handle']);
    $router->get('/r/statuses', ['uses' => 'Driver\GetRideByStatusController@handle']);
    $router->get('/r/bing', ['uses' => 'Driver\GetActiveRideController@handle']);
});


/**
 * Driver Ride Routes
 */
$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:admin'
            ]
    ], function ($router) {

    $router->get('/admins', ['uses' => 'Admin\GetRidesController@handle']);
});


