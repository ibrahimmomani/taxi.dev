<?php

namespace Sto\Modules\Ride\Exceptions;

use Sto\Services\Core\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RideFailedException
 * @package Sto\Modules\Ride\Exceptions
 */
class RideFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Ride.';
}
