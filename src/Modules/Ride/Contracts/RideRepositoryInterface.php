<?php

namespace Sto\Modules\Ride\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RideRepositoryInterface.
 *
 * 
 */
interface RideRepositoryInterface extends RepositoryInterface
{

}
