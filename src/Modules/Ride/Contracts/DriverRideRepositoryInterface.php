<?php

namespace Sto\Modules\Ride\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DriverRideRepositoryInterface
 * @package Sto\Modules\Ride\Contracts
 */
interface DriverRideRepositoryInterface extends RepositoryInterface
{

}
