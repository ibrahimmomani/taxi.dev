<?php

namespace Sto\Modules\Rider\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="RiderLocationSuccessResponse",
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="object"
 *      ),
 * )
 */
class RiderLocationSuccessResponse
{

}