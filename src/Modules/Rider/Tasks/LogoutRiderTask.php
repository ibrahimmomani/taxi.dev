<?php

namespace Sto\Modules\Rider\Tasks;

use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class LogoutTask.
 *
 * 
 */
class LogoutRiderTask extends Task
{

    /**
     * @var \Sto\Modules\Rider\Tasks\AuthenticationService|\Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * LogoutTask constructor.
     *
     * @param \Sto\Services\Authentication\Portals\AuthenticationService $authenticationService
     */
    public function __construct(
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param $authorizationHeader
     *
     * @return bool
     */
    public function run($authorizationHeader)
    {
        $ok = $this->authenticationService->logout($authorizationHeader);

        return $ok;
    }
}
