<?php

namespace Sto\Modules\Rider\Tasks;

use Illuminate\Support\Facades\Hash;
use Sto\Modules\Rider\Exceptions\AccountUpdateException;
use Sto\Modules\Rider\Requests\MeRiderChangePasswordRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class MeRiderChangePasswordTask
 * @package Sto\Modules\Rider\Tasks
 */
class MeRiderChangePasswordTask extends Task
{
    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;
    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * MeRiderChangePasswordTask constructor.
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
    }

    /**
     * Change password.
     * 
     * @param MeRiderChangePasswordRequest $request
     * @return mixed
     */
    public function run(MeRiderChangePasswordRequest $request)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        if(!Hash::check($request->password, $user->password))
        {
            throw new AccountUpdateException();
        }
        $hashedNewPassword = Hash::make($request->newPassword);

        if($this->userRepository->update(['password' => $hashedNewPassword],$user->id))
        {
            $token = $this->authenticationService->refreshToken();
            $user->injectToken($token);
            return $user;
        }
    }

}