<?php

namespace Sto\Modules\Rider\Tasks;

use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Services\Core\Task\Abstracts\Task;

class MeRiderDetailsTask extends Task
{
    private $authenticationService;

    public function __construct(
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param $authorizationHeader
     *
     * @return bool
     */
    public function run()
    {
        return $this->authenticationService->getAuthenticatedUser();
    }
}