<?php

namespace Sto\Modules\Rider\Tasks;

use Sto\Modules\Rider\Requests\MeRiderLocationsRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class MeRiderLocationsTask
 * @package Sto\Modules\Rider\Tasks
 */
class MeRiderLocationsTask extends Task
{
      /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;
    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * MeRiderChangePasswordTask constructor.
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param MeRiderLocationsRequest $request
     * @return mixed
     */
    public function run(MeRiderLocationsRequest $request)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        return $this->userRepository->update($request->all(),$user->id);
    }
}