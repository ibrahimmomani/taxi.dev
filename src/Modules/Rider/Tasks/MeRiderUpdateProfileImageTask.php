<?php

namespace Sto\Modules\Rider\Tasks;


use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Rider\Requests\MeRiderUpdateProfileImageRequest;
use Antennaio\Clyde\ClydeUpload;
use Antennaio\Clyde\Facades\ClydeImage;

class MeRiderUpdateProfileImageTask extends Task
{

    private $authenticationService;

    private $userRepository;

    protected $uploads;

    /**
     * MeRiderUpdateProfileImageTask constructor.
     * @param UserRepositoryInterface $userRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService,
        ClydeUpload $uploads
    ) {
        $this->userRepository = $userRepository;
        $this->authenticationService = $authenticationService;
        $this->uploads = $uploads;
    }

    /**
     * @param MeRiderUpdateProfileImageRequest $meRiderUpdateProfileImageRequest
     * @return mixed
     */
    public function run(MeRiderUpdateProfileImageRequest $meRiderUpdateProfileImageRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        if ($meRiderUpdateProfileImageRequest->hasFile('picture_url')) {
            $filename = $this->uploads->upload($meRiderUpdateProfileImageRequest->file('picture_url'));
            $data = ['picture_url' => ClydeImage::url($filename, ['p' => 'small'])];
            return $this->userRepository->update($data, $user->id);
        }
    }

}