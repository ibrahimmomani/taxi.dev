<?php

namespace Sto\Modules\Rider\Tests\Api;

use Sto\Services\Core\Test\Abstracts\TestCase;

/**
 * Class LoginEndpointTest.
 *
 * 
 */
class LoginRiderTest extends TestCase
{

    private $endpoint = '/login';

    public function testLoginExistingRider_()
    {
        $userDetails = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
            'phone_number' => '+15915544326',

        ];

        // get the logged in user (create one if no one is logged in)
        $this->registerAndLoginTestingUser($userDetails);

        $data = [
            'email'    => $userDetails['email'],
            'password' => $userDetails['password'],
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '200');

        // assert the response contain the expected data
        $this->assertResponseContainKeyValue([
            'email' => $userDetails['email'],
            'name'  => $userDetails['name'],
        ], $response);

        // assert response contain the data
        $this->assertResponseContainKeys(['id', 'token'], $response);
    }

    public function testLoginExistingRiderUsingGetRequest()
    {
        $data = [
            'email'    => 'mega@mail.dev',
            'password' => 'secret',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'get', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '405');

        // assert message is correct
        $this->assertResponseContainKeyValue([
            'message' => '405 Method Not Allowed',
        ], $response);
    }

    public function testLoginNonExistingRider()
    {
        $data = [
            'email'    => 'i-do-not-exist@mail.dev',
            'password' => 'secret',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '401');

        // assert message is correct
        $this->assertResponseContainKeyValue([
            'message' => 'Credentials Incorrect.',
        ], $response);
    }

    public function testLoginExistingRiderWithoutEmail_()
    {
        $userDetails = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
            'phone_number' => '+15915544326',
        ];

        // get the logged in user (create one if no one is logged in)
        $this->registerAndLoginTestingUser($userDetails);

        $data = [
            'password' => $userDetails['password'],
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert message is correct
        $this->assertValidationErrorContain($response, [
            'email' => 'The email field is required.',
        ]);
    }

    public function testLoginExistingRiderWithoutPassword()
    {
        $userDetails = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
            'phone_number' => '+15915544326',
        ];

        // get the logged in user (create one if no one is logged in)
        $this->registerAndLoginTestingUser($userDetails);

        $data = [
            'email' => $userDetails['email'],
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert message is correct
        $this->assertValidationErrorContain($response, [
            'password' => 'The password field is required.',
        ]);
    }

    public function testLoginExistingRiderWithoutEmailAndPassword()
    {
        $userDetails = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
            'phone_number' => '+15915544326',
        ];

        // get the logged in user (create one if no one is logged in)
        $this->registerAndLoginTestingUser($userDetails);

        $data = []; // empty data

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert message is correct
        $this->assertValidationErrorContain($response, [
            'email'    => 'The email field is required.',
            'password' => 'The password field is required.',
        ]);
    }

}
