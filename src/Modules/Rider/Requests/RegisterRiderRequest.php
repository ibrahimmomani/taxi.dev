<?php

namespace Sto\Modules\Rider\Requests;

use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class RegisterRequest.
 *
 * 
 */
class RegisterRiderRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email|max:40|unique:users',
            'password' => 'required|min:6|max:30',
            'name'     => 'required|min:2|max:50',
            'phone_number' => 'required|unique:users',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
