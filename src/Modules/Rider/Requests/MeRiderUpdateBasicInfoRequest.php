<?php

namespace Sto\Modules\Rider\Requests;

use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class MeRiderUpdateBasicInfoRequest
 * @package Sto\Modules\Rider\Requests
 */
class MeRiderUpdateBasicInfoRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:6|max:50|filled',
            'locale'     => 'min:2|max:2|filled'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}