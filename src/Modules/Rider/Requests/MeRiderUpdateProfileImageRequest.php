<?php


namespace Sto\Modules\Rider\Requests;


use Sto\Services\Core\Request\Abstracts\Request;

class MeRiderUpdateProfileImageRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture_url'=>'image'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}