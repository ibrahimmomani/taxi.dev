<?php

namespace Sto\Modules\Rider\Controllers\Api;


use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Rider\Requests\MeRiderUpdateProfileImageRequest;
use Sto\Modules\Rider\Tasks\MeRiderUpdateProfileImageTask;
use Sto\Modules\Rider\Transformers\RiderTransformer;

class MeRiderUpdateProfileImage extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/riders/me/settings/picture",
     *      summary="Change picture",
     *      tags={"Rider Account"},
     *      consumes={"multipart/form-data"},
     *      @SWG\Parameter(
     *         description="file to upload",
     *         in="formData",
     *         name="picture_url",
     *         required=false,
     *         type="file"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiderUpdateResponse"
     *              )
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */
    public function handle
    (
        MeRiderUpdateProfileImageRequest $meRiderUpdateProfileImageRequest,
        MeRiderUpdateProfileImageTask $meRiderUpdateProfileImageTask
    )
    {
        $result = $meRiderUpdateProfileImageTask->run($meRiderUpdateProfileImageRequest);
        return $this->response->item($result, new RiderTransformer());
    }

}