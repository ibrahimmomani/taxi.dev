<?php

namespace Sto\Modules\Rider\Controllers\Api;

use Sto\Modules\Rider\Requests\RegisterRiderRequest;
use Sto\Modules\Rider\Tasks\CreateRiderTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class RegisterController.
 *
 * 
 */
class RegisterRiderController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/riders/register",
     *      summary="Register a rider",
     *      tags={"Authentication Rider"},
     *      description="Register a rider and log him in by generating his token",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          default="{""name"":""John Doe"",""email"":""rider@maued.dev"",""password"":""1234567"",""phone_number"":""+9721231232""}",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="name",
     *                  description="name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="email",
     *                  description="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  description="password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="phone_number",
     *                  description="phone_number",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="latitude",
     *                  description="latitude",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="longitude",
     *                  description="longitude",
     *                  type="string"
     *              ),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiderSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */

    /**
     * @param RegisterRiderRequest $registerRequest
     * @param CreateRiderTask $createRiderTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        RegisterRiderRequest $registerRequest,
        CreateRiderTask $createRiderTask
    ) {
        $user = $createRiderTask->run(
            $registerRequest['email'],
            $registerRequest['password'],
            $registerRequest['name'],
            $registerRequest['phone_number'],
            true
        );

        return $this->response->item($user, new UserTransformer());
    }
}
