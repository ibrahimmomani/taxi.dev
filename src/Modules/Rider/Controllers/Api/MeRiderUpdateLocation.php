<?php

namespace Sto\Modules\Rider\Controllers\Api;

use Sto\Modules\Rider\Transformers\RiderLocationTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Rider\Requests\MeRiderLocationsRequest;
use Sto\Modules\Rider\Tasks\MeRiderLocationsTask;

/**
 * Class MeRiderUpdateLocation
 * @package Sto\Modules\Rider\Controllers\Api
 */
class MeRiderUpdateLocation extends ApiController
{
       /**
     * @SWG\Put(
     *      path="/riders/me/settings/locations",
     *      summary="Update Rider Location",
     *      tags={"Rider Account"},
     *      description="Update rider location latitude and longitude",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
         *                  property="latitude",
     *                  description="latitude",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="longitude",
     *                  description="longitude Password",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiderLocationSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */

    /**
     * @param MeRiderLocationsRequest $locationsRequest
     * @param MeRiderLocationsTask $locationsTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        MeRiderLocationsRequest $locationsRequest,
        MeRiderLocationsTask $locationsTask
    ) {
        $result = $locationsTask->run($locationsRequest);
        return $this->response->item($result, new RiderLocationTransformer());
    }
}