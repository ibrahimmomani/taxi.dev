<?php

namespace Sto\Modules\Rider\Controllers\Api;

use Sto\Modules\Rider\Tasks\LogoutRiderTask;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Services\Core\Request\Manager\HttpRequest;

/**
 * Class LogoutController.
 *
 * 
 */
class LogoutRiderController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/riders/me/logout",
     *      summary="Logout rider",
     *      tags={"Rider Account"},
     *      description="Logout rider and revoke jwt token.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="authorization",
     *          type="string",
     *          in="header",
     *          description=""
     *      ),
     *      @SWG\Response(
     *          response=202,
     *          description="Accepted.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="Revoked and blacklisted token.",
     *     )
     * )
     */
    /**
     * @param HttpRequest $request
     * @param LogoutRiderTask $logoutRiderTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(HttpRequest $request, LogoutRiderTask $logoutRiderTask)
    {
        $logoutRiderTask->run($request->header('authorization'));

        return $this->response->accepted(null, [
            'message' => 'Rider Logged Out Successfully.',
        ]);
    }
}
