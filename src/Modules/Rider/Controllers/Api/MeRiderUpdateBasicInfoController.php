<?php

namespace Sto\Modules\Rider\Controllers\Api;

use Sto\Modules\Rider\Transformers\RiderTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Rider\Requests\MeRiderUpdateBasicInfoRequest;
use Sto\Modules\Rider\Tasks\MeRiderUpdateBasicInfoTask;
/**
 * Class MeRiderUpdateBasicInfoController
 * @package Sto\Modules\Rider\Controllers\Api
 */
class MeRiderUpdateBasicInfoController extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/riders/me/settings/basic",
     *      summary="Change Rider Basic Info",
     *      tags={"Rider Account"},
     *      description="Change rider basic information such as name and locale",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="name",
     *                  description="name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="locale",
     *                  description="locale",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiderUpdateResponse"
     *              )
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */
    /**
     * @param MeRiderUpdateBasicInfoRequest $basicInfoRequest
     * @param MeRiderUpdateBasicInfoTask $basicInfoTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        MeRiderUpdateBasicInfoRequest $basicInfoRequest,
        MeRiderUpdateBasicInfoTask $basicInfoTask
    )
    {
        $result = $basicInfoTask->run($basicInfoRequest);
        return $this->response->item($result, new RiderTransformer());
    }
}