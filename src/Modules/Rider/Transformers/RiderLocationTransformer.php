<?php

namespace Sto\Modules\Rider\Transformers;

use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class RiderLocationTransformer
 * @package Sto\Modules\Rider\Transformers
 */
class RiderLocationTransformer extends Transformer
{

    /**
     * @param \Sto\Modules\User\Models\User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'user_id' => (int) $user->id,
            'location'=>
                [
                'latitude' => $user->latitude,
                'longitude' => $user->longitude
                ]
        ];
    }
}