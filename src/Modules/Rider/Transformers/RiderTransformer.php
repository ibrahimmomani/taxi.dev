<?php

namespace Sto\Modules\Rider\Transformers;

use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class UserTransformer.
 *
 * 
 */
class RiderTransformer extends Transformer
{

    /**
     * @param \Sto\Modules\User\Models\User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'user_id' => (int) $user->id,
            'name' => $user->name,
            'locale' => $user->locale,
            'picture_url' => $user->picture_url,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ];
    }
}
