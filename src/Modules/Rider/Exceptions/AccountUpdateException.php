<?php

namespace Sto\Modules\Rider\Exceptions;

use Sto\Services\Core\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccountUpdateException.
 *
 *
 */
class AccountUpdateException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Updateing Rider.';
}
