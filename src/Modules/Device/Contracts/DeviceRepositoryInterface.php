<?php

namespace Sto\Modules\Device\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DeviceRepositoryInterface.
 *
 * 
 */
interface DeviceRepositoryInterface extends RepositoryInterface
{

}
