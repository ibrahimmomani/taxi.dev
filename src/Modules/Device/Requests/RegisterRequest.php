<?php

namespace Sto\Modules\Device\Requests;

use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class RegisterRequest.
 *
 *
 */
class RegisterRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'  => '',
            'registraion_id' => '',
            'os'=>'filled|in:IOS,Android',
            'os_version' => '',
            'app_version' => '',
            'app_type' => 'filled|in:Driver,Rider',
            'device_model' => '',
            'location_lat' => '',
            'location_lon' => '',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
