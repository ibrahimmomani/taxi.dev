<?php

namespace Sto\Modules\Device\Definitions\Models;

/**
 * @SWG\Definition(
 *      definition="Device",
 *      @SWG\Property(
 *          property="registraion_id",
 *          description="registraion_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="os",
 *          description="os",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="os_version",
 *          description="os_version",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="app_version",
 *          description="app_version",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="app_type",
 *          description="app_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_model",
 *          description="device_model",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lat",
 *          description="latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lon",
 *          description="longitude",
 *          type="string"
 *      )
 * )
 */
class Device
{
}