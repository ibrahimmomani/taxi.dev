<?php

namespace Sto\Modules\User\Tasks;

use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Core\Repository\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class ListAllUsersTask.
 *
 * 
 */
class ListAllUsersTask extends Task
{

    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * ListAllUsersTask constructor.
     *
     * @param \Sto\Modules\User\Contracts\UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        $this->userRepository->pushCriteria(new OrderByCreationDateDescendingCriteria());

        $users = $this->userRepository->paginate();

        return $users;
    }
}
