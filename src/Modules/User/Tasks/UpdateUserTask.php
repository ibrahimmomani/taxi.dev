<?php

namespace Sto\Modules\User\Tasks;

use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Authentication\Exceptions\UpdateResourceFailedException;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class UpdateUserTask.
 *
 * 
 */
class UpdateUserTask extends Task
{

    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UpdateUserTask constructor.
     *
     * @param \Sto\Modules\User\Contracts\UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param      $userId
     * @param null $password
     * @param null $name
     *
     * @return mixed
     */
    public function run($userId, $password = null, $name = null)
    {
       
        // check if data is empty
        if (!$password && !$name) {
            throw new UpdateResourceFailedException('All inputs are empty.');
        }

        $attributes = [
            'password' => $password,
            'name'     => $name,
        ];

        // updating the attributes
        $user = $this->userRepository->update($attributes, $userId);

        return $user;
    }
}
