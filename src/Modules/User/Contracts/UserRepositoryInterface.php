<?php

namespace Sto\Modules\User\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepositoryInterface.
 *
 * 
 */
interface UserRepositoryInterface extends RepositoryInterface
{

}
