<?php

namespace Sto\Modules\User\Repositories\Criterias\Eloquent;

use Sto\Services\Core\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class DriverNearByCriteria extends Criteria
{
    private $lat;
    private $lon;
    private $rad;

    /**
     * UserWithDistanceCriteria constructor.
     * @param $lat
     * @param $lon
     * @param $rad
     */
    public function __construct($lat, $lon, $rad)
    {
        $this->lat = $lat;
        $this->lon = $lon;
        $this->rad = $rad;
    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {

          return $model::filterByLocationAndDistance($this->lat, $this->lon, $this->rad)->whereExists(function ($query) {
                $query->select(\DB::raw("*"))
                    ->from("roles")
                    ->join("role_user", "roles.id" ,"=", "role_user.role_id")
                    ->whereRaw("role_user.user_id = users.id")
                    ->whereRaw("name = 'driver'");
            });


    }

}