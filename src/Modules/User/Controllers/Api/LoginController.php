<?php

namespace Sto\Modules\User\Controllers\Api;

use Sto\Modules\User\Requests\LoginRequest;
use Sto\Modules\User\Tasks\LoginTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class LoginController.
 *
 * 
 */
class LoginController extends ApiController
{

    /**
     * @param \Sto\Modules\User\Requests\LoginRequest $loginRequest
     * @param \Sto\Modules\User\Tasks\LoginTask       $loginTask
     *
     * @return \Dingo\Api\Http\Response
     */
    public function handle(LoginRequest $loginRequest, LoginTask $loginTask)
    {
        $user = $loginTask->run($loginRequest['email'], $loginRequest['password']);

        return $this->response->item($user, new UserTransformer());
    }
}
