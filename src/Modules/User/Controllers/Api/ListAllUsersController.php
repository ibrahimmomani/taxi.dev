<?php

namespace Sto\Modules\User\Controllers\Api;

use Sto\Modules\User\Tasks\ListAllUsersTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class ListAllUsersController.
 *
 * 
 */
class ListAllUsersController extends ApiController
{

    /**
     * @param \Sto\Modules\User\Tasks\ListAllUsersTask $listAllUsersTask
     *
     * @return \Dingo\Api\Http\Response
     */
    public function handle(ListAllUsersTask $listAllUsersTask)
    {
        $users = $listAllUsersTask->run();

        return $this->response->paginator($users, new UserTransformer());
    }
}
