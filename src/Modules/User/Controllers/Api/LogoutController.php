<?php

namespace Sto\Modules\User\Controllers\Api;

use Sto\Modules\User\Tasks\LogoutTask;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Services\Core\Request\Manager\HttpRequest;

/**
 * Class LogoutController.
 *
 * 
 */
class LogoutController extends ApiController
{

    /**
     * @param \Sto\Services\Core\Request\Manager\HttpRequest $request
     * @param \Sto\Modules\User\Tasks\LogoutTask             $logoutTask
     *
     * @return \Dingo\Api\Http\Response
     */
    public function handle(HttpRequest $request, LogoutTask $logoutTask)
    {
        $logoutTask->run($request->header('authorization'));

        return $this->response->accepted(null, [
            'message' => 'User Logged Out Successfully.',
        ]);
    }
}
