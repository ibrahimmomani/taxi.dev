<?php

namespace Sto\Modules\User\Tests\Api;

use Sto\Services\Core\Test\Abstracts\TestCase;

/**
 * Class DeleteUserTest.
 *
 * 
 */
class DeleteUserTest extends TestCase
{

    private $endpoint = '/users';

    public function testDeleteExistingUser_()
    {
        $user = $this->getLoggedInTestingUser();


        $data = [
            'name' => 'Updated Name',
            'password' => 'updated#Password',
        ];

        $endpoint = $this->endpoint . '/' . $user->id;
        // send the HTTP request
        $response = $this->apiCall($endpoint, 'delete', $data);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '202');

        // assert the returned message is correct
        $this->assertResponseContainKeyValue([
            'message' => 'User (' . $user->id . ') Deleted Successfully.',
        ], $response);
    }

    public function testDeleteDifferentUser()
    {
        $endpoint = $this->endpoint . '/' . 100; // any ID
        // send the HTTP request
        $response = $this->apiCall($endpoint, 'delete');
        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '403');

        // assert user not allowed to proceed with the request
        $this->assertEquals($response->getContent(), '{"message":"Forbidden","status_code":403}');

    }
}
