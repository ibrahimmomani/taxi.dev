<?php

namespace Sto\Modules\User\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Sto\Modules\Driver\Models\Car;
use Sto\Modules\Ride\Models\DriverRide;
use Sto\Modules\Ride\Models\Ride;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;
use Sto\Services\Authentication\Portals\TokenTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User.
 *
 * 
 */
class User extends Authenticatable implements JWTSubject

{

    use  TokenTrait, EntrustUserTrait;


    // use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_number',
        'remeber_token',
        'picture_url',
        'latitude',
        'longitude',
        'available',
        'locale',
        'is_active'

    ];

    /**
     * The dates attributes.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {

        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    public function rideForRider()
    {
        return $this->hasMany(Ride::class);
    }

    public function rideForDriver()
    {
        return $this->hasMany(DriverRide::class);
    }

    public function scopeFilterByLocationAndDistance($query, $latitude, $longitude, $distance,  $unit = "km")
    {
        $unit = ($unit === "km") ? 6378.10 : 3963.17;
        $latitude = (double)$latitude;
        $longitude = (double)$longitude;

        $haversine = "($unit * acos(cos(radians($latitude)) * cos(radians(users.latitude)) * cos(radians(users.longitude) - radians($longitude)) + sin(radians($latitude)) * sin(radians(users.latitude))))";

        return $query->selectRaw("*, {$haversine} AS distance")
            ->orderBy('distance', 'asc')
            ->whereRaw("{$haversine} < ?", [$distance])
            ->whereRaw("available = 1");
    }

}
