<?php

namespace Sto\Modules\User\Transformers;

use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class UserTransformer.
 *
 *
 */
class UserTransformer extends Transformer
{

    /**
     * @param \Sto\Modules\User\Models\User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [    
            'user_id' => (int)$user->id,
            'name' => $user->name,
            'email' => $user->email,
            'token' => $user->token,
            'phone' => $user->phone_number,
            'picture_url' => $user->picture_url,
            'role' => ($user->roles()->first()) ? $user->roles()->first()->name : '',
            'latitude' => $user->latitude,
            'longitude' => $user->longitude,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ];
    }
}
