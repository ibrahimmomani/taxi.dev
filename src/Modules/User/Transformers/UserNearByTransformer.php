<?php


namespace Sto\Modules\User\Transformers;

use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;
use Sto\Services\GeoTools\GeoTools;

class UserNearByTransformer extends Transformer
{
    /**
     * @param \Sto\Modules\User\Models\User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'user_id' => (int)$user->id,
            'name' => $user->name,
            'distance' => ($user->distance) ? ceil($user->distance) : 0,
            'eta' => ($user->distance) ? ceil(GeoTools::getEtaByDistance($user->distance)) : null,
            'role' => ($user->roles()->first()) ? $user->roles()->first()->name : '',
            'phone' => $user->phone_number,
            'picture_url'=>$user->picture_url,
            'car' => [
                'car_type' => ($user->cars->first()) ? $user->cars->first()->type->type : null,
                'car_number' => ($user->cars->first()) ? $user->cars->first()->car_number : null,
                'car_model' => ($user->cars->first()) ? $user->cars->first()->car_model : null,
                'car_year' => ($user->cars->first()) ? $user->cars->first()->car_year : null,
            ],
            'latitude' => $user->latitude,
            'longitude' => $user->longitude,
        ];
    }
}