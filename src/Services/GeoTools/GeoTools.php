<?php

namespace Sto\Services\GeoTools;

use \Toin0u\Geotools\Facade\Geotools;

class GeoTools
{
    public static function getDistance($a_latitude, $a_longitude, $b_latitude, $b_longitude)
    {
        $coordA   = Geotools::coordinate([$a_latitude, $a_longitude]);
        $coordB   = Geotools::coordinate([$b_latitude, $b_longitude]);

        $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

        return $distance->in('km')->haversine();
    }

    public static function getEta($a_latitude, $a_longitude, $b_latitude, $b_longitude, $speed=45)
    {
        return self::getDistance($a_latitude, $a_longitude, $b_latitude, $b_longitude) / $speed * 60;
    }

    public static function getEtaByDistance($distance, $speed=45)
    {
        return ($distance / $speed) * 60;
    }
}