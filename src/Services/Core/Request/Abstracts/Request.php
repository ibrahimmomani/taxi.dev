<?php

namespace Sto\Services\Core\Request\Abstracts;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest as LaravelFrameworkRequest;
use Sto\Services\Core\Exception\Exceptions\ValidationFailedException;
use Sto\Services\Core\Exception\Exceptions\AuthorizationFailedException;


/**
 * Class Request.
 *
 * 
 */
abstract class Request extends LaravelFrameworkRequest
{

    /**
     * overriding the failedValidation function to throw my custom
     * exception instead of the default Laravel exception.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return mixed|void
     */
    public function failedValidation(Validator $validator)
    {

        throw new ValidationFailedException($validator->getMessageBag());
    }

    public function failedAuthorization()
    {
        throw new AuthorizationFailedException();

    }

}
