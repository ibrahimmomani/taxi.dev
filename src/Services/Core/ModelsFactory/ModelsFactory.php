<?php

// User
$factory->define(Sto\Modules\User\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name'     => $faker->name,
        'email'    => $faker->email,
        'password' => bcrypt(str_random(10)),
        'phone_number' => $faker->phoneNumber
    ];
});

// ...
