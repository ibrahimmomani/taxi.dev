<?php

namespace Sto\Services\Core\Model\Abstracts;

use Illuminate\Database\Eloquent\Model as LaravelEloquentModel;

/**
 * Class Model.
 *
 * 
 */
abstract class Model extends LaravelEloquentModel
{

}
