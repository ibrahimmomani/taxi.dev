<?php

namespace Sto\Services\Core\Command\Abstracts;

use Illuminate\Contracts\Bus\SelfHandling as LaravelSelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs as LaravelDispatchesJobs;
use Sto\Services\Core\Command\Traits\DispatcherTrait;

/**
 * Class Command.
 *
 * 
 */
abstract class Command implements LaravelSelfHandling
{

    use LaravelDispatchesJobs;
    use DispatcherTrait;
}
