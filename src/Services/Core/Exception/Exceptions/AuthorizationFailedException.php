<?php

namespace Sto\Services\Core\Exception\Exceptions;

use Sto\Services\Core\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class ValidationFailedException.
 *
 * Note: exceptionally extending from `Dingo\Api\Exception\ResourceException` instead of
 * `Sto\Services\Core\Exception\Abstracts\ApiException`. To keep the request validation
 * throwing well formatted error. To be debugged later and switched to extending from
 * `ApiException` while carefully looking at the validation response error format.
 *
 * 
 */
class AuthorizationFailedException extends ApiException
{

    public $httpStatusCode = SymfonyResponse::HTTP_FORBIDDEN;

    public $message = 'Forbidden';


}
