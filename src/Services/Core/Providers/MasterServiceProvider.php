<?php

namespace Sto\Services\Core\Providers;

use Sto\Modules\Device\Providers\DeviceServiceProvider;
use Sto\Modules\Admin\Providers\AdminServiceProvider;
use Sto\Modules\Driver\Providers\DriverServiceProvider;
use Sto\Modules\Rider\Providers\RiderServiceProvider;
use Sto\Modules\Ride\Providers\RideServiceProvider;
use Sto\Modules\User\Providers\UserServiceProvider;
use Sto\Services\Core\Providers\Abstracts\ServiceProvider;
use Sto\Services\Core\Route\Providers\ApiBaseRouteServiceProvider;

/**
 * Class MasterServiceProvider
 * The main Service Provider where all Service Providers gets registered
 * this is the only Service Provider that gets injected in the Config/app.php.
 *
 * Class MasterServiceProvider
 *
 *
 */
class MasterServiceProvider extends ServiceProvider
{
    /**
     * Application Service Provides.
     *
     * @var array
     */
    private $serviceProviders = [
        ApiBaseRouteServiceProvider::class,
        // Modules Service Providers:
        UserServiceProvider::class,
        RiderServiceProvider::class,
        DriverServiceProvider::class,
        AdminServiceProvider::class,
        RideServiceProvider::class,
        DeviceServiceProvider::class,
        // ...
    ];

    public function boot()
    {
        foreach ($this->serviceProviders as $serviceProvider) {
            $this->app->register($serviceProvider);
        }

        $this->overrideDefaultFractalSerializer();
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->changeTheDefaultDatabaseModelsFactoriesPath();
        $this->debugDatabaseQueries(true);
    }
}