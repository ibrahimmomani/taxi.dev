<?php

namespace Sto\Services\Core\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Sto\Services\Authorization\Seeders\SeedRolesAndPermissions;
use Sto\Modules\Ride\Seeders\SeedRideStatusesAndCarTypes;

/**
 * Class DatabaseSeeder.
 *
 * 
 */
class DatabaseSeeder extends Seeder
{

    /**
     * The application Seeders that needs to be registered.
     *
     * @var array
     */
    protected $seeders = [
        SeedRolesAndPermissions::class,
        SeedRideStatusesAndCarTypes::class
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach ($this->seeders as $seeder) {
            $this->call($seeder);
        }

        Model::reguard();
    }
}
