<?php

namespace Sto\Services\Authorization\Models;

use Zizaco\Entrust\EntrustPermission;

/**
 * Class Permission
 *
 * 
 */
class Permission extends EntrustPermission
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];
}
