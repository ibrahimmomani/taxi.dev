<?php

namespace Sto\Services\Authorization\Models;

use Zizaco\Entrust\EntrustRole;

/**
 * Class Role
 *
 * 
 */
class Role extends EntrustRole
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];
}
