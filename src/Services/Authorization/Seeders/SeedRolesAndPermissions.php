<?php

namespace Sto\Services\Authorization\Seeders;

use Illuminate\Database\Seeder;
use Sto\Services\Authorization\Models\Permission;
use Sto\Services\Authorization\Models\Role;

class SeedRolesAndPermissions extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'Administrator';
        $admin->save();

        $rider = new Role();
        $rider->name = 'rider';
        $rider->display_name = 'Rider';
        $rider->save();

        $driver = new Role();
        $driver->name = 'driver';
        $driver->display_name = 'Driver';
        $driver->save();

        $listUsers = new Permission();
        $listUsers->name = 'list-users';
        $listUsers->display_name = 'List all Users';
        $listUsers->save();

        $admin->attachPermission($listUsers);
    }
}