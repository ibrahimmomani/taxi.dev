<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->integer('user_id')->unsigned();
            $table->decimal('latitude_from', 10, 8)->nullable();
            $table->decimal('longitude_from', 10, 8)->nullable();
            $table->text('from_txt');
            $table->decimal('latitude_to', 10, 8)->nullable();
            $table->decimal('longitude_to', 10, 8)->nullable();
            $table->text('to_txt');
            $table->decimal('fare');
            $table->timestamp('pickup_time');
            $table->enum('people', [1,2,3,4])->default(1);
            $table->integer('ride_status_id')->default(1);
            $table->integer('car_type_id')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('driver_rides', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('user_id')->unsigned();
            $table->integer('ride_status_id')->default(4);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::Create('ride_statuses', function(Blueprint $table){
            $table->increments('id');
            $table->string('status', 20);
            $table->timestamps();
        });

        Schema::Create('car_types', function(Blueprint $table){
            $table->increments('id');
            $table->string('type', 60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('rides');
        Schema::drop('ride_statuses');
        Schema::drop('car_types');
        Schema::drop('driver_rides');
    }
}
